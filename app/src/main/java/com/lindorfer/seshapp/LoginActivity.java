package com.lindorfer.seshapp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.lindorfer.seshapp.classes.UserProfile;
import com.lindorfer.seshapp.main.MainActivity;
import com.lindorfer.seshapp.queries.Query;
import com.lindorfer.seshapp.queries.RequestValues;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    SharedPreferences sharedPref;
    String token;
    String username;

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    // UI references.
    private AutoCompleteTextView mUsernameView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        // Set up the login form.
        mUsernameView = (AutoCompleteTextView) findViewById(R.id.username);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        // Register button
        Button btRegister = findViewById(R.id.btRegister);
        btRegister.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://lindronics.pythonanywhere.com/accounts/register"));
                startActivity(browserIntent);
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        checkAuthenticated();
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        this.username = mUsernameView.getText().toString().toLowerCase();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(username)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            focusView = mUsernameView;
            cancel = true;
        } else if (!isEmailValid(username)) {
            mUsernameView.setError(getString(R.string.error_invalid_email));
            focusView = mUsernameView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            System.out.println("AUTH DETAILS: " + username + "; " + password);
            showProgress(true);
            mAuthTask = new UserLoginTask(username, password, this);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return true;
        //return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return true;
        //return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
 
    /**
     * Check if user already authenticated
     */
    private void checkAuthenticated(){

        // Check if authentication token provided
        token = sharedPref.getString("token", null);
        username = sharedPref.getString("username", null);
        System.out.println("TOKEN: " + token + ", USERNAME: " + username);

        // If token provided, get user profile and go to main activity
        if (token != null && username != null){
            showProgress(true);
            GetProfileTask task = new GetProfileTask(token, this.username, this);
            task.execute((Void) null);
        }
    }

    /**
     * Start main activity
     */
    private void startMain(){
        Intent mainIntent = new Intent(this, MainActivity.class);
        startActivity(mainIntent);
        finish();
    }


    /**
     * Represents an asynchronous task used to verify the token
     */
    public class GetProfileTask extends AsyncTask<Void, Void, Boolean> implements RequestValues {

        private final String token;
        private final String username;
        private LoginActivity activity;

        GetProfileTask(String token, String username, LoginActivity activity) {
            this.token = token;
            this.username = username;
            this.activity = activity;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            // Send HTTP request
            String url = SERVER_URL + "get-user";
            try {
                String response = Query.post(url, this.username, this.token);

                // Parse response into UserProfile object
                JSONObject element = new JSONObject(response);

                int id = Integer.parseInt(element.get("id").toString());
                String rank = element.get("rank").toString();
                String spiritDrink = element.get("spirit_drink").toString();
                String username = element.get("username").toString();

                UserProfile profile = new UserProfile(this.token, username, spiritDrink, rank, id);

                // Store UserProfile
                Gson gson = new Gson();
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("user_profile", gson.toJson(profile, UserProfile.class));
                editor.apply();

            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                System.out.println("AUTH SUCCESS");
                activity.startMain();
            } else {
                mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> implements RequestValues {

        private final String mPassword;
        private final String mUsername;
        private LoginActivity mActivity;

        UserLoginTask(String username, String password, LoginActivity activity) {
            this.mUsername = username;
            this.mPassword = password;
            this.mActivity = activity;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            // Setup HTTP POST parameters from server
            Map<String,String> authParams = new HashMap<>();
            authParams.put("username", this.mUsername);
            authParams.put("password", this.mPassword);

            // Send HTTP request
            String url = SERVER_URL + "token-auth";
            JSONObject request = new JSONObject(authParams);

            try {
                String response = Query.post(url, request.toString());

                // Store token
                JSONObject tokenObject = new JSONObject(response);

                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("token", tokenObject.getString("token"));
                editor.putString("username", this.mUsername);
                editor.apply();

            } catch(Exception e) {
                return false;
            }

            return true;
        }

        @Override
        protected void onPostExecute(final Boolean response) {
            mAuthTask = null;
            showProgress(false);

            if (response) {
                mActivity.checkAuthenticated();
            } else {
                mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}

