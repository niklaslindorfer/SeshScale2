package com.lindorfer.seshapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.lindorfer.seshapp.classes.Sesh;
import com.lindorfer.seshapp.classes.SeshUser;
import com.lindorfer.seshapp.classes.UserProfile;
import com.lindorfer.seshapp.exceptions.QueryException;
import com.lindorfer.seshapp.queries.QueryCaller;
import com.lindorfer.seshapp.queries.QueryManager;
import com.lindorfer.seshapp.queries.RequestType;
import com.lindorfer.seshapp.queries.RequestURL;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;

public class CreateSeshActivity extends AppCompatActivity implements QueryCaller {

    protected UserProfile user;
    protected SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_sesh);

        // Initialise sharedPref, get user profile info
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Gson gson = new Gson();
        this.user = gson.fromJson(sharedPref.getString("user_profile", null), UserProfile.class);

        // Create sesh button
        FloatingActionButton btCreateSesh = findViewById(R.id.btCreateSesh);
        btCreateSesh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createSesh();
            }
        });
    }

    private void createSesh(){
        TextView lbLocation = findViewById(R.id.lbLocation);
        TextView lbDescription = findViewById(R.id.lbDescription);

        // Online server request
        try{
            // Prepare request body
            JSONObject request = new JSONObject();
            request.put("user", user.getId());
            request.put("location", lbLocation.getText());
            request.put("description", lbDescription.getText());

            // Send request
            QueryManager q = new QueryManager(RequestType.POST, user, RequestURL.CREATE_SESH, request.toString(), this);
            q.execute();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (QueryException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Intent i = new Intent();
        i.putExtra("success",true);
        setResult(101,i);
        finish();
    }

    @Override
    public void getServerResponse(String response){
        try {

            // TODO: horrible spaghetti code here
            JSONObject element = new JSONObject(response);

            JSONObject seshStructure = element.getJSONObject("sesh");
            int seshId = Integer.parseInt(seshStructure.get("id").toString());
            String loc = seshStructure.get("location").toString();
            String desc = seshStructure.get("description").toString();
            String date = seshStructure.get("date").toString();

            JSONObject hostStructure = element.getJSONObject("user");

            int hostId = Integer.parseInt(hostStructure.get("id").toString());
            boolean host = Boolean.parseBoolean(hostStructure.get("host").toString());
            boolean chunder = Boolean.parseBoolean(hostStructure.get("chunder").toString());
            boolean blackout = Boolean.parseBoolean(hostStructure.get("blackout").toString());

            // TODO: completely unnecessary
            Sesh sesh = new Sesh(seshId, desc, loc, date);
            SeshUser seshUser = new SeshUser(sesh, user, host, chunder, blackout, true);
            System.out.println("NEW SESH: " + sesh);
            System.out.println("NES SESH USER: " + seshUser);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showProgress(Boolean show) {

    }
}
