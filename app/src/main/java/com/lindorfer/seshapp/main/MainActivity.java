package com.lindorfer.seshapp.main;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.google.gson.Gson;
import com.lindorfer.seshapp.LoginActivity;
import com.lindorfer.seshapp.R;
import com.lindorfer.seshapp.classes.SeshUser;
import com.lindorfer.seshapp.classes.UserProfile;
import com.lindorfer.seshapp.main.mainFragments.AccountFragment;
import com.lindorfer.seshapp.main.mainFragments.FriendSeshFragment;
import com.lindorfer.seshapp.main.mainFragments.HomeFragment;

public class MainActivity extends AppCompatActivity {

    public SharedPreferences sharedPref;
    private UserProfile user;
    private Toolbar tbMain;
    private ViewPager viewPager;
    private MenuItem prevMenuItem;

    /**
     *  Bottom navigation handler
     */
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    viewPager.setCurrentItem(0);
                    return true;
                case R.id.navigation_dashboard:
                    viewPager.setCurrentItem(1);
                    return true;
                case R.id.navigation_notifications:
                    viewPager.setCurrentItem(2);
                    return true;
            }
            return false;
        }
    };

    /**
     * On app start
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialise components
        final BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // Initialise shared preferences
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        getUserProfile();

        // Setup toolbar
        tbMain = findViewById(R.id.tbMain);
        tbMain.setTitle("  The Sesh");
        tbMain.setTitleTextColor(Color.WHITE);

        // Get the ViewPager and set it's PagerAdapter so that it can display items
        viewPager = findViewById(R.id.vpMainTabs);
        viewPager.setAdapter(new MainFragmentPagerAdapter(getSupportFragmentManager(), this));
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener(){
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {
                if (prevMenuItem != null) {
                    prevMenuItem.setChecked(false);
                }
                else {
                    navigation.getMenu().getItem(0).setChecked(false);
                }
                navigation.getMenu().getItem(position).setChecked(true);
                prevMenuItem = navigation.getMenu().getItem(position);
            }
            @Override
            public void onPageScrollStateChanged(int state) {}
        });

    }

    //TODO: somehow refactor this with fragment code
    /**
     * Retrieves current UserProfile from shared preferences.
     */
    protected void getUserProfile(){
        // Get user profile info
        Gson gson = new Gson();
        this.user = gson.fromJson(sharedPref.getString("user_profile", null), UserProfile.class);
    }

    public void logout(){
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove("token");
        editor.remove("username");
        editor.remove("user_profile");
        editor.apply();

        Intent loginIntent = new Intent(this, LoginActivity.class);
        startActivity(loginIntent);
        finish();
    }


    @SuppressWarnings("MissingSuperCall")
    @Override
    public void onBackPressed(){
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
    }

    class MainFragmentPagerAdapter extends FragmentPagerAdapter {
        final int PAGE_COUNT = 3;
        private String tabTitles[] = new String[] { "home", "friend_sesh", "account" };
        private Context context;

        public MainFragmentPagerAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.context = context;
        }

        public MainFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public Fragment getItem(int position) {
            Gson gson = new Gson();
            switch(position){
                case 2:
                    return AccountFragment.newInstance();
                case 1:
                    return FriendSeshFragment.newInstance();
                case 0:
                    return HomeFragment.newInstance();
                default:
                    return HomeFragment.newInstance();
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            return tabTitles[position];
        }
    }
}
