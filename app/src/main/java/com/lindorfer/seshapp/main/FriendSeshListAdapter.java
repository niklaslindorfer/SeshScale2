package com.lindorfer.seshapp.main;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lindorfer.seshapp.R;
import com.lindorfer.seshapp.classes.SeshUser;
import com.lindorfer.seshapp.main.mainFragments.MainFragment;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nikla on 31/03/2018.
 */

public class FriendSeshListAdapter extends ArrayAdapter<SeshUser> {
    private final Context context;
    private List<SeshUser> seshUserList;
    private MainFragment fragment;

    public FriendSeshListAdapter(Context context, List<SeshUser> seshUserList, MainFragment frag) {
        super(context, -1, seshUserList);
        this.context = context;
        this.seshUserList = seshUserList;
        this.fragment = frag;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.friend_sesh_list_row_layout, parent, false);

        TextView lbFriendSeshName = rowView.findViewById(R.id.lbFriendSeshName);
        TextView lbFriendSeshDescription = rowView.findViewById(R.id.lbFriendSeshDescription);
        TextView lbFriendSeshLocation = rowView.findViewById(R.id.lbFriendSeshLocation);
        TextView lbFriendSeshActive = rowView.findViewById(R.id.lbFriendSeshActive);
        TextView lbFriendSeshId = rowView.findViewById(R.id.lbFriendSeshId);

        SeshUser friendSesh = seshUserList.get(position);

        lbFriendSeshName.setText(friendSesh.getUser().getUsername());
        lbFriendSeshDescription.setText(friendSesh.getSesh().getDescription());
        lbFriendSeshLocation.setText(friendSesh.getSesh().getLocation());
        lbFriendSeshId.setText(String.valueOf(friendSesh.getSesh().getId()));

        // Set joined sesh
        String seshList = fragment.getSharedPref().getString("sesh_list", null);
        Gson gson = new Gson();
        Type listType = new TypeToken<ArrayList<SeshUser>>(){}.getType();
        ArrayList<SeshUser> joinedSeshes = gson.fromJson(seshList, listType);
        boolean isJoined = false;
        for (SeshUser joinedSesh : joinedSeshes) {
            if (joinedSesh.getSesh().getId() == friendSesh.getSesh().getId())
                isJoined = true;
        }

        lbFriendSeshActive.setText(isJoined ? "JOINED" : "NOT JOINED");
        rowView.setClickable(isJoined);

        return rowView;
    }

}
