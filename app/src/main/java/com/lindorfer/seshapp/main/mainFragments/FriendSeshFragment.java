package com.lindorfer.seshapp.main.mainFragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.lindorfer.seshapp.CreateSeshActivity;
import com.lindorfer.seshapp.R;
import com.lindorfer.seshapp.classes.Sesh;
import com.lindorfer.seshapp.classes.SeshUser;
import com.lindorfer.seshapp.classes.UserProfile;
import com.lindorfer.seshapp.exceptions.QueryException;
import com.lindorfer.seshapp.main.FriendSeshListAdapter;
import com.lindorfer.seshapp.main.SeshListAdapter;
import com.lindorfer.seshapp.queries.QueryCaller;
import com.lindorfer.seshapp.queries.QueryManager;
import com.lindorfer.seshapp.queries.RequestType;
import com.lindorfer.seshapp.queries.RequestURL;
import com.lindorfer.seshapp.seshDetails.SeshDetailsActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FriendSeshFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FriendSeshFragment extends MainFragment implements QueryCaller {

    private List<SeshUser> friendSeshList;
    private ArrayAdapter<SeshUser> itemsAdapter;
    private ListView lvFriendSesh;
    private SwipeRefreshLayout srRefreshFriendSeshList;

    public FriendSeshFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FriendSeshFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FriendSeshFragment newInstance() {
        FriendSeshFragment fragment = new FriendSeshFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_friend_sesh, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();

        // List view
        lvFriendSesh = getActivity().findViewById(R.id.lvFriendSesh);
        lvFriendSesh.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Get selected item
                final SeshUser seshUser = itemsAdapter.getItem(position);

                // Ask for join confirmation
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(getContext(), android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(getContext());
                }
                builder.setTitle("Join sesh")
                        .setMessage("Are you sure you want to join this sesh?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                joinSesh(seshUser);
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(R.drawable.ic_add_white_24dp)
                        .show();
            }
        });

        friendSeshList = new ArrayList<>();

        // Swipe to update
        srRefreshFriendSeshList = getActivity().findViewById(R.id.srRefreshFriendSeshList);
        srRefreshFriendSeshList.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getFriendSeshList();
                    }
                }
        );
        srRefreshFriendSeshList.setProgressViewOffset(false, 60, 200);

        // Get sesh list
        getFriendSeshList();
    }

    private void joinSesh(SeshUser seshUser){
                // Online server request
        try{
            // Prepare request body
            JSONObject requestObject = new JSONObject();
            requestObject.put("sesh", seshUser.getSesh().getId());
            requestObject.put("username", user.getUsername());
            String request = requestObject.toString();

            // Send request
            QueryManager q = new QueryManager(RequestType.POST, user, RequestURL.JOIN_SESH, request, this);
            q.execute();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (QueryException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getFriendSeshList(){
        // Online server request
        try{
            // Prepare request body
            String request = user.getUsername();

            // Send request
            QueryManager q = new QueryManager(RequestType.POST, user, RequestURL.GET_FRIEND_SESH_LIST, request, this);
            q.execute();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (QueryException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getServerResponse(String response) {
        if (response != null) {
            if (response.equals("SUCCESS")) {
                getFriendSeshList();
            } else {
                try {
                    friendSeshList = new ArrayList<>();
                    JSONArray responseArray = new JSONArray(response);
                    for (int i = 0; i < responseArray.length(); i++) {
                        JSONObject element = responseArray.getJSONObject(i);

                        // Handle sesh
                        int seshId = Integer.parseInt(element.getString("sesh_id"));
                        String location = element.getString("sesh_location");
                        String description = element.getString("sesh_description");

                        Sesh sesh = new Sesh(seshId, description, location);

                        // Handle user profile
                        int userId = Integer.parseInt(element.getString("id"));
                        String username = element.getString("username");
                        String rank = element.getString("rank");
                        String spiritDrink = element.getString("spirit_drink");

                        UserProfile friend = new UserProfile(username, spiritDrink, rank, userId);

                        // Handle sesh user
                        SeshUser seshUser = new SeshUser(sesh, friend);

                        friendSeshList.add(seshUser);
                    }

                    itemsAdapter = new FriendSeshListAdapter(getActivity(),friendSeshList, this);
                    lvFriendSesh.setAdapter(itemsAdapter);

                    srRefreshFriendSeshList.setRefreshing(false);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    @Override
    public void showProgress(final Boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);


            final ProgressBar mProgressView = getActivity().findViewById(R.id.login_progress);
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });

            final ListView lvFriendSesh = getActivity().findViewById(R.id.lvFriendSesh);
            lvFriendSesh.setVisibility(show ? View.GONE : View.VISIBLE);
            lvFriendSesh.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    lvFriendSesh.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });
        }
    }
}
