package com.lindorfer.seshapp.main.mainFragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.google.gson.Gson;
import com.lindorfer.seshapp.R;
import com.lindorfer.seshapp.classes.UserProfile;

/**
 * Created by nikla on 31/03/2018.
 * Fragment on main activity.
 */
public abstract class MainFragment extends Fragment {

    protected UserProfile user;
    protected SharedPreferences sharedPref;

    public MainFragment() {

    }

    /**
     * Retrieves current UserProfile from shared preferences.
     */
    protected void getUserProfile(){
        // Get user profile info
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        Gson gson = new Gson();
        this.user = gson.fromJson(sharedPref.getString("user_profile", null), UserProfile.class);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        getUserProfile();
        super.onCreate(savedInstanceState);
    }

    protected void writeSharedPref(String key, String value) {
        // Write to sharedPref
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.commit();
    }


    public UserProfile getUser() {
        return user;
    }

    public void setUser(UserProfile user) {
        this.user = user;
    }

    public SharedPreferences getSharedPref() {
        return sharedPref;
    }

    public void setSharedPref(SharedPreferences sharedPref) {
        this.sharedPref = sharedPref;
    }
}