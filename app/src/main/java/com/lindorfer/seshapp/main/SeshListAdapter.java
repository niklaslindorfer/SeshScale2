package com.lindorfer.seshapp.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.lindorfer.seshapp.classes.SeshUser;
import com.lindorfer.seshapp.R;

import java.util.List;

/**
 * Created by nikla on 31/03/2018.
 */

public class SeshListAdapter extends ArrayAdapter<SeshUser> {
    private final Context context;
    private List<SeshUser> seshUserList;

    public SeshListAdapter(Context context, List<SeshUser> seshUserList) {
        super(context, -1, seshUserList);
        this.context = context;
        this.seshUserList = seshUserList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.sesh_list_row_layout, parent, false);

        TextView lbSeshId = rowView.findViewById(R.id.lbSeshId);
        TextView lbSeshDescription = rowView.findViewById(R.id.lbSeshDescription);
        TextView lbSeshLocation = rowView.findViewById(R.id.lbSeshLocation);
        TextView lbSeshActive = rowView.findViewById(R.id.lbSeshActive);


        lbSeshId.setText(String.valueOf(seshUserList.get(position).getSesh().getId()));
        lbSeshDescription.setText(seshUserList.get(position).getSesh().getDescription());
        lbSeshLocation.setText(seshUserList.get(position).getSesh().getLocation());
        lbSeshActive.setText(seshUserList.get(position).isActive() ? "ACTIVE" : "INACTIVE");

        return rowView;
    }

}
