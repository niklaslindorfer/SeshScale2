package com.lindorfer.seshapp.main.mainFragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.lindorfer.seshapp.main.MainActivity;
import com.lindorfer.seshapp.R;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AccountFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AccountFragment extends MainFragment {
    TextView lbUsername;
    TextView lbRank;
    TextView lbSpiritDrink;
    Button btLogout;

    public AccountFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment AccountFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AccountFragment newInstance() {
        AccountFragment fragment = new AccountFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        // Initialise views
        lbUsername = getActivity().findViewById(R.id.lbUsername);
        lbRank = getActivity().findViewById(R.id.lbRank);
        lbSpiritDrink = getActivity().findViewById(R.id.lbSpiritDrink);
        btLogout = getActivity().findViewById(R.id.btLogout);

        // Populate views
        lbUsername.setText(user.getUsername());
        lbRank.setText(user.getRank());
        lbSpiritDrink.setText(user.getSpiritDrink());

        // Set logout
        btLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).logout();
            }
        });

        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_account, container, false);
    }
}
