package com.lindorfer.seshapp.main.mainFragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Fragment;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.lindorfer.seshapp.CreateSeshActivity;
import com.lindorfer.seshapp.R;
import com.lindorfer.seshapp.classes.Sesh;
import com.lindorfer.seshapp.classes.SeshUser;
import com.lindorfer.seshapp.exceptions.QueryException;
import com.lindorfer.seshapp.main.SeshListAdapter;
import com.lindorfer.seshapp.queries.QueryCaller;
import com.lindorfer.seshapp.queries.QueryManager;
import com.lindorfer.seshapp.queries.RequestType;
import com.lindorfer.seshapp.queries.RequestURL;
import com.lindorfer.seshapp.seshDetails.SeshDetailsActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends MainFragment implements QueryCaller {

    private List<SeshUser> seshUserList;
    private ArrayAdapter<SeshUser> itemsAdapter;
    private ListView lvSeshUsers;
    SwipeRefreshLayout srRefreshSeshList;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment HomeFragment.
     */
    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();

        // List view
        lvSeshUsers = getActivity().findViewById(R.id.lvSeshUsers);
        lvSeshUsers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Get selected item
                SeshUser seshUser = itemsAdapter.getItem(position);

                // Prepare sesh to send to details activity
                Gson gson = new Gson();
                String argument = gson.toJson(seshUser);

                // Jump to sesh details activity
                Intent seshDetailsIntent = new Intent(getActivity(), SeshDetailsActivity.class);
                seshDetailsIntent.putExtra("sesh_user", argument);
                startActivity(seshDetailsIntent);
            }
        });

        // Swipe to update
        srRefreshSeshList = getActivity().findViewById(R.id.srRefreshSeshList);
        srRefreshSeshList.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getSeshList();
                    }
                }
        );
        srRefreshSeshList.setProgressViewOffset(false, 60, 200);

        // Floating action button to create new sesh
        FloatingActionButton btNewSesh = getActivity().findViewById(R.id.btNewSesh);
        btNewSesh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newSeshIntent = new Intent(getActivity(), CreateSeshActivity.class);
                startActivity(newSeshIntent);
            }
        });

        seshUserList = new ArrayList<>();

        // Get sesh list
        getSeshList();

    }


    private void getSeshList(){

        // Online server request
        try{
            // Prepare request body
            String request = user.getUsername();

            // Send request
            QueryManager q = new QueryManager(RequestType.POST, user, RequestURL.GET_SESH_LIST, request, this);
            q.execute();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (QueryException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getServerResponse(String response) {
        try {
            seshUserList = new ArrayList<>();
            JSONArray responseArray = new JSONArray(response);
            for (int i = 0; i < responseArray.length(); i++) {
                JSONObject element = responseArray.getJSONObject(i);

                // Handle sesh
                JSONObject seshObj = element.getJSONObject("sesh");
                int seshId = Integer.parseInt(seshObj.getString("id"));
                String location = seshObj.getString("location");
                String description = seshObj.getString("description");
                String date = seshObj.getString("date");
                Sesh sesh = new Sesh(seshId, description, location, date);

                // Handle user profile
                JSONObject userObj = element.getJSONObject("user");
                //int userId = Integer.parseInt(element.getString("id"));
                Boolean host = Boolean.parseBoolean(userObj.getString("host"));
                Boolean chunder = Boolean.parseBoolean(userObj.getString("chunder"));
                Boolean blackout = Boolean.parseBoolean(userObj.getString("blackout"));
                Boolean active = Boolean.parseBoolean(userObj.getString("active"));
                SeshUser seshUser = new SeshUser(sesh, user, host, chunder, blackout, active);

                seshUserList.add(seshUser);
            }

            // Write to sharedPref
            Gson gson = new Gson();
            writeSharedPref("sesh_list", gson.toJson(seshUserList));

            itemsAdapter = new SeshListAdapter(getActivity(), seshUserList);
            lvSeshUsers.setAdapter(itemsAdapter);

            srRefreshSeshList.setRefreshing(false);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    @Override
    public void showProgress(final Boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);


            final ProgressBar mProgressView = getActivity().findViewById(R.id.login_progress);
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });

            final ListView lvSeshUsers = getActivity().findViewById(R.id.lvSeshUsers);
            lvSeshUsers.setVisibility(show ? View.GONE : View.VISIBLE);
            lvSeshUsers.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    lvSeshUsers.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });
        }
    }
}
