package com.lindorfer.seshapp.queries;

/**
 * Created by nikla on 31/03/2018.
 */

public interface QueryCaller {
    void getServerResponse(String response);
    void showProgress(final Boolean show);
}
