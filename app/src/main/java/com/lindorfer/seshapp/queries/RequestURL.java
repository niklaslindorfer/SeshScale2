package com.lindorfer.seshapp.queries;

/**
 * Created by nikla on 06/04/2018.
 */

public enum RequestURL {
    GET_TOKEN("token-auth"),
    CREATE_SESH("create-sesh"),
    GET_SESH_LIST("get-sesh-list"),
    GET_STATE_LOGS("get-state-logs"),
    GET_DRINK_LOGS("get-drink-logs"),
    GET_DRINK_LIST("get-drink-list"),
    ADD_STATE("add-condition"),
    ADD_DRINK_LOG("add-drink"),
    GET_USER_PROFILE("get-user"),
    GET_USER_PROFILE_PICTURE("get-picture"),
    GET_ACTIVE_SESH("get-active-sesh"),
    END_ACTIVE_SESH("end-active-sesh"),
    GET_FRIEND_LIST("get-friend-list"),
    GET_FRIEND_SESH_LIST("get-join-sesh-list"),
    JOIN_SESH("join-sesh"),
    UPDATE_SESH("update-sesh");

    private String url;

    RequestURL(String url){
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
