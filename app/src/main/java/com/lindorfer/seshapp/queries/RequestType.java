package com.lindorfer.seshapp.queries;

/**
 * Created by nikla on 31/03/2018.
 */

public enum RequestType {
    POST, GET
}
