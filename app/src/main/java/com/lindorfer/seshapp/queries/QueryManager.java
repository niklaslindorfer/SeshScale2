package com.lindorfer.seshapp.queries;

import android.os.AsyncTask;

import com.lindorfer.seshapp.classes.UserProfile;
import com.lindorfer.seshapp.exceptions.QueryException;

import java.net.MalformedURLException;

/**
 * Created by nikla on 31/03/2018.
 */

public class QueryManager extends AsyncTask<Void, Void, String> implements RequestValues {

    private UserProfile user;
    private QueryCaller caller;
    private String url;
    private String request;
    private RequestType requestType;

    /**
     * Creates QueryManager for HTTP POST.
     * @param requestType must be POST.
     * @param user UserProfile for call
     * @param url sub-URL on server
     * @param request Request body
     * @param caller Calling activity
     * @throws MalformedURLException
     * @throws QueryException
     */
    public QueryManager(RequestType requestType, UserProfile user, RequestURL url, String request, QueryCaller caller) throws MalformedURLException, QueryException{
        if (requestType != RequestType.POST){
            throw new QueryException("Only POST request possible");
        } else {
            this.user = user;
            this.caller = caller;
            this.url = SERVER_URL + url.getUrl();
            this.request = request;
            this.requestType = requestType;
            this.caller.showProgress(true);
        }
    }

    /**
     * Creates QueryManager for HTTP GET
     * @param requestType must be GET
     * @param user UserProfile for call
     * @param url sub-URL on server
     * @param caller Calling activity
     * @throws MalformedURLException
     * @throws QueryException
     */
    public QueryManager(RequestType requestType, UserProfile user, RequestURL url, QueryCaller caller) throws MalformedURLException, QueryException{
        if (requestType != RequestType.GET){
            throw new QueryException("Only GET request possible");
        } else {
            this.user = user;
            this.caller = caller;
            this.url = SERVER_URL + url.getUrl();
            this.requestType = requestType;
            this.caller.showProgress(true);
        }
    }

    private void showProgress(Boolean show){
        this.caller.showProgress(show);
    }

    @Override
    protected String doInBackground(Void... params) {

        // Send HTTP request
        String response = null;
        try {
            if (requestType == RequestType.POST){
                response = Query.post(url, request, user.getToken());
            } else if (requestType == RequestType.GET){
                response = Query.get(url, user.getToken());
            }


        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
        return response;
    }

    @Override
    protected void onPostExecute(final String response) {
        showProgress(false);
        caller.getServerResponse(response);
    }

    @Override
    protected void onCancelled() {

    }
}
