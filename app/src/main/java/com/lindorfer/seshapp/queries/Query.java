package com.lindorfer.seshapp.queries;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import com.lindorfer.seshapp.exceptions.QueryException;
import com.lindorfer.seshapp.exceptions.ServerException;

public class Query {

    /**
     * Creates a HTTP POST request and returns the response from the server.
     * @param urlString The URL for the query as a String
     * @param query The content of the query as a String
     * @return The server response as a String
     */
    public static String post(String urlString, String query) throws IOException, ServerException, QueryException{
        String response = "";
        try{
            URL url = new URL(urlString);
            
            // Prepare HTTP connection
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            
            // Prepare output streams and writers
            BufferedOutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
            
            // Write to request and connect
            writer.write(query);
            
            writer.flush();
            writer.close();
            out.close();
            urlConnection.connect();

            // Await response
            int responseCode = urlConnection.getResponseCode();
            System.out.println("HTTP RESPONSE CODE: " + responseCode);

            // If response is OK, read data
            if (responseCode == HttpURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                while ((line=br.readLine()) != null) {
                    response += line;
                }
            } else {
                throw new ServerException("No connection to server!");
            }
        } catch(IOException e) {
            // E.g. not reachable
            throw e;
        } catch (ServerException e) {
            // Server side error
            throw e;
        } catch(Exception e){
            e.printStackTrace();
            // Everything else
            throw new QueryException("Something went wrong.");
        }
        System.out.println("HTTP RESPONSE VALUE: " + response);
        return response;
    }

    /**
     * Creates a HTTP POST request and returns the response from the server.
     * @param urlString The URL for the query as a String
     * @param query The content of the query as a String
     * @param token Authentication token as a String
     * @return The server response as a String
     */
    public static String post(String urlString, String query, String token) throws IOException, ServerException, QueryException{
        String response = "";
        try{
            URL url = new URL(urlString);

            // Prepare HTTP connection
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            urlConnection.setRequestProperty("Authorization", "Token " + token);
            
            // Prepare output streams and writers
            BufferedOutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
            
            // Write to request and connect
            writer.write(query);
            
            writer.flush();
            writer.close();
            out.close();
            urlConnection.connect();

            // Await response
            int responseCode = urlConnection.getResponseCode();
            System.out.println("HTTP RESPONSE CODE: " + responseCode);

            // If response is OK, read data
            if (responseCode == HttpURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                while ((line=br.readLine()) != null) {
                    response += line;
                }
            } else {
                throw new ServerException("No connection to server!");
            }
        } catch(IOException e) {
            // E.g. not reachable
            throw e;
        } catch (ServerException e) {
            // Server side error
            throw e;
        } catch(Exception e){
            // Everything else
            throw new QueryException("Something went wrong.");
        }
        System.out.println("HTTP RESPONSE VALUE: " + response);
        return response;
    }

    /**
     * Creates a HTTP GET request and returns the response from the server.
     * @param urlString The URL for the query as a String
     * @param token Authentication token as a String.
     * @return The server response as a String
     */
    public static String get(String urlString, String token) throws IOException, ServerException, QueryException{

        // TODO overload method for parameters

        String response = "";
        try{
            URL url = new URL(urlString);
            
            // Prepare HTTP connection
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            urlConnection.setRequestProperty("Authorization", "Token " + token);
            
            // Connect
            urlConnection.connect();

            // Await response
            int responseCode = urlConnection.getResponseCode();
            System.out.println("HTTP RESPONSE CODE: " + responseCode);

            // If response is OK, read data
            if (responseCode == HttpURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                while ((line=br.readLine()) != null) {
                    response += line;
                }
            } else {
                throw new ServerException("No connection to server!");
            }
        } catch(IOException e) {
            // E.g. not reachable
            throw e;
        } catch (ServerException e) {
            // Server side error
            throw e;
        } catch(Exception e){
            // Everything else
            throw new QueryException("Something went wrong.");
        }
        System.out.println("HTTP RESPONSE VALUE: " + response);
        return response;
    }
    
}
