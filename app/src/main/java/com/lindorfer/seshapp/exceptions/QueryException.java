package com.lindorfer.seshapp.exceptions;


/**
 * Created by nikla on 30/03/2018.
 */

public class QueryException extends Exception{

    public QueryException(){
        super();
    }

    public QueryException(String message){
        super(message);
    }
}
