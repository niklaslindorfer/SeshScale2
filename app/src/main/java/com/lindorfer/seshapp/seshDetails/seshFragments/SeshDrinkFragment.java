package com.lindorfer.seshapp.seshDetails.seshFragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.lindorfer.seshapp.R;
import com.lindorfer.seshapp.classes.Drink;
import com.lindorfer.seshapp.classes.DrinkLog;
import com.lindorfer.seshapp.classes.SeshUser;
import com.lindorfer.seshapp.classes.StateLog;
import com.lindorfer.seshapp.exceptions.QueryException;
import com.lindorfer.seshapp.main.mainFragments.MainFragment;
import com.lindorfer.seshapp.queries.QueryCaller;
import com.lindorfer.seshapp.queries.QueryManager;
import com.lindorfer.seshapp.queries.RequestType;
import com.lindorfer.seshapp.queries.RequestURL;
import com.lindorfer.seshapp.seshDetails.AddDrinkActivity;
import com.lindorfer.seshapp.seshDetails.AddStateActivity;
import com.lindorfer.seshapp.seshDetails.DrinkLogListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.net.MalformedURLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SeshDrinkFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SeshDrinkFragment extends MainFragment implements QueryCaller{

    private static final String ARG_SESH = "sesh_arg";
    private SeshUser seshUser;

    private ListView lvDrinkLogs;
    private List<DrinkLog> drinkLogs;
    private ArrayAdapter<DrinkLog> itemsAdapter;
    private double unitSum;
    private TextView lbUnitSum;

    public SeshDrinkFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param sesh JSON SeshUser.
     * @return A new instance of fragment SeshDrinkFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SeshDrinkFragment newInstance(String sesh) {
        SeshDrinkFragment fragment = new SeshDrinkFragment();
        Bundle args = new Bundle();
        args.putString(ARG_SESH, sesh);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Gson gson = new Gson();
            seshUser = gson.fromJson(getArguments().getString(ARG_SESH), SeshUser.class);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sesh_drink, container, false);
    }

    @Override
    public void onResume(){
        super.onResume();

        System.out.println(">>> SESH DRINK FRAGMENT");

        Gson gson = new Gson();
        seshUser = gson.fromJson(getArguments().getString(ARG_SESH), SeshUser.class);

        lvDrinkLogs = getActivity().findViewById(R.id.lvDrinkLogs);
        lbUnitSum = getActivity().findViewById(R.id.lbUnitSum);

        drinkLogs = new ArrayList<>();
        getDrinkLogs();

        // Floating action button to create new state log
        FloatingActionButton btNewDrinkLog = getActivity().findViewById(R.id.btNewDrinkLog);
        btNewDrinkLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newDrinkIntent = new Intent(getActivity(), AddDrinkActivity.class);
                Gson gson = new Gson();
                newDrinkIntent.putExtra("sesh_user", gson.toJson(seshUser, SeshUser.class));
                startActivity(newDrinkIntent);
            }
        });

    }

    private void getDrinkLogs(){
        // Online server request
        try{
            // Prepare request body
            JSONObject request = new JSONObject();
            request.put("username", user.getUsername());
            request.put("sesh", seshUser.getSesh().getId());

            System.out.println("REQUEST: " + request.toString());

            // Send request
            QueryManager q = new QueryManager(RequestType.POST, user, RequestURL.GET_DRINK_LOGS, request.toString(), this);
            q.execute();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (QueryException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getServerResponse(String response) {
        unitSum = 0;
        try {
            JSONArray responseArray = new JSONArray(response);
            for (int i = 0; i < responseArray.length(); i++) {
                JSONObject element = responseArray.getJSONObject(i);

                System.out.println(element.toString());
                double units = Double.parseDouble(element.getString("units"));
                int drinkId = Integer.parseInt(element.getString("drink_id"));
                int logId = Integer.parseInt(element.getString("id"));
                String dateTime = element.getString("date_time");
                String name = element.getString("name");
                String type = element.getString("drink_type");

                Drink drink = new Drink(drinkId, name, units, type);
                drinkLogs.add(new DrinkLog(drink, dateTime, logId));
                unitSum += drink.getUnits();
            }

            itemsAdapter = new DrinkLogListAdapter(getActivity(), drinkLogs);
            lvDrinkLogs.setAdapter(itemsAdapter);

            lbUnitSum.setText(new DecimalFormat("#.##").format(unitSum) + " UK units in " + drinkLogs.size() + " drinks");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    @Override
    public void showProgress(final Boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);


            final ProgressBar mProgressView = getActivity().findViewById(R.id.login_progress);
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });

            final ViewPager vpSeshTabs = getActivity().findViewById(R.id.vpSeshTabs);
            vpSeshTabs.setVisibility(show ? View.GONE : View.VISIBLE);
            vpSeshTabs.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    vpSeshTabs.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });
        }

    }
}
