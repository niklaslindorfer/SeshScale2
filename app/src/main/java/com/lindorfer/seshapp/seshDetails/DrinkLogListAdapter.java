package com.lindorfer.seshapp.seshDetails;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lindorfer.seshapp.R;
import com.lindorfer.seshapp.classes.Drink;
import com.lindorfer.seshapp.classes.DrinkLog;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by nikla on 31/03/2018.
 */

public class DrinkLogListAdapter extends ArrayAdapter<DrinkLog> {
    private final Context context;
    private List<DrinkLog> drinkLogList;

    public DrinkLogListAdapter(Context context, List<DrinkLog> drinkLogList) {
        super(context, -1, drinkLogList);
        this.context = context;
        this.drinkLogList = drinkLogList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.drink_log_list_row_layout, parent, false);

        TextView lbDrinkName = rowView.findViewById(R.id.lbDrinkName);
        TextView lbUnits = rowView.findViewById(R.id.lbUnits);
        TextView lbLogTime = rowView.findViewById(R.id.lbLogTime);
        ImageView ivDrinkIcon = rowView.findViewById(R.id.ivDrinkIcon);

        DrinkLog item = drinkLogList.get(position);

        lbDrinkName.setText(item.getDrink().getName());
        lbUnits.setText(item.getDrink().getUnits() + " UK units");

        // Time
        Duration duration = new Duration(item.getDate(), new DateTime());

        // TODO: ugly as fuck
        String timeString = "Logged ";
        long minutes = duration.getStandardMinutes();
        if (minutes>=60) {
            long hours = duration.getStandardHours();
            timeString += hours;
            minutes -= hours * 60;
            timeString += " h and ";
        }
        timeString += minutes + " m ago.";

        lbLogTime.setText(timeString);

        // Icon
        if (item.getDrink().getType().equals("pint"))
            ivDrinkIcon.setImageResource(R.drawable.ic_beer);
        else if (item.getDrink().getType().equals("mix"))
            ivDrinkIcon.setImageResource(R.drawable.ic_cocktail);
        else if (item.getDrink().getType().equals("can"))
            ivDrinkIcon.setImageResource(R.drawable.ic_can);
        else if (item.getDrink().getType().equals("bottle"))
            ivDrinkIcon.setImageResource(R.drawable.ic_bottle);
        else if (item.getDrink().getType().equals("shot"))
            ivDrinkIcon.setImageResource(R.drawable.ic_shot);

        return rowView;
    }

}
