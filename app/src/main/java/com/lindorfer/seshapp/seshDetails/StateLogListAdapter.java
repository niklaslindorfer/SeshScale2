package com.lindorfer.seshapp.seshDetails;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lindorfer.seshapp.R;
import com.lindorfer.seshapp.classes.DrinkLog;
import com.lindorfer.seshapp.classes.StateLog;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.util.List;

/**
 * Created by nikla on 31/03/2018.
 */

public class StateLogListAdapter extends ArrayAdapter<StateLog> {
    private final Context context;
    private List<StateLog> stateLogList;

    public StateLogListAdapter(Context context, List<StateLog> drinkLogList) {
        super(context, -1, drinkLogList);
        this.context = context;
        this.stateLogList = drinkLogList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.state_log_list_row_layout, parent, false);

        TextView lbStateMental = rowView.findViewById(R.id.lbStateMental);
        TextView lbStatePhysical = rowView.findViewById(R.id.lbStatePhysical);
        TextView lbStateTime = rowView.findViewById(R.id.lbStateTime);

        StateLog item = stateLogList.get(position);

        lbStateMental.setText("Mental: " + item.getMental());
        lbStatePhysical.setText("Physical: " + item.getPhysical());

        // Time
        Duration duration = new Duration(item.getDate(), new DateTime());

        // TODO: ugly as fuck
        String timeString = "Logged ";
        long minutes = duration.getStandardMinutes();
        if (minutes>=60) {
            long hours = duration.getStandardHours();
            timeString += hours;
            minutes -= hours * 60;
            timeString += " h and ";
        }
        timeString += minutes + " m ago.";

        lbStateTime.setText(timeString);

        return rowView;
    }

}
