package com.lindorfer.seshapp.seshDetails;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;
import android.support.v7.widget.Toolbar;

import com.google.gson.Gson;
import com.lindorfer.seshapp.R;
import com.lindorfer.seshapp.classes.SeshUser;
import com.lindorfer.seshapp.classes.UserProfile;
import com.lindorfer.seshapp.exceptions.QueryException;
import com.lindorfer.seshapp.queries.QueryCaller;
import com.lindorfer.seshapp.queries.QueryManager;
import com.lindorfer.seshapp.queries.RequestType;
import com.lindorfer.seshapp.queries.RequestURL;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;

public class AddStateActivity extends AppCompatActivity implements QueryCaller{

    private SeshUser seshUser;
    private SharedPreferences sharedPref;
    private UserProfile user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_state);

        // Get sesh from Intent call
        Intent intent = getIntent();
        String argument = intent.getStringExtra("sesh_user");

        // Toolbar
        Toolbar tbNewStateLog = findViewById(R.id.tbNewStateLog);
        tbNewStateLog.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        tbNewStateLog.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // Parse to object
        Gson gson = new Gson();
        seshUser = gson.fromJson(argument, SeshUser.class);
        System.out.println("SESH USER RETRIEVED: " + seshUser);

        // Get current user TODO: maybe refactor
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        this.user = gson.fromJson(sharedPref.getString("user_profile", null), UserProfile.class);
    }


    /***
     * This method gets the current sesh state of the user and adds it to the activated sesh.
     */
    public void submitCondition(View view){

        // Get physical state
        int physical = 0;
        RadioGroup physicalGroup = findViewById(R.id.physical);
        switch(physicalGroup.getCheckedRadioButtonId()){
            case R.id.p0: physical = 0; break;
            case R.id.p1: physical = 1; break;
            case R.id.p2: physical = 2; break;
            case R.id.p3: physical = 3; break;
            case R.id.p4: physical = 4; break;
            case R.id.p5: physical = 5; break;
        }

        // Get mental state
        int mental = 0;
        RadioGroup mentalGroup = findViewById(R.id.mental);
        switch(mentalGroup.getCheckedRadioButtonId()){
            case R.id.m0: mental = 0; break;
            case R.id.m1: mental = 1; break;
            case R.id.m2: mental = 2; break;
            case R.id.m3: mental = 3; break;
            case R.id.mn1: mental = -1; break;
            case R.id.mn2: mental = -2; break;
            case R.id.mn3: mental = -3; break;
        }

        // Online server request
        try{
            JSONObject request = new JSONObject();
            request.put("sesh", seshUser.getSesh().getId());
            request.put("user", seshUser.getUser().getUsername());
            request.put("rated_by", seshUser.getUser().getId());
            request.put("physical", physical);
            request.put("mental", mental);

            QueryManager q = new QueryManager(RequestType.POST, user, RequestURL.ADD_STATE, request.toString(), this);
            q.execute();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (QueryException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void getServerResponse(String response) {
        if (response.equals("SUCCESS")){
            finish();
        }
    }

    @Override
    public void showProgress(Boolean show) {

    }
}
