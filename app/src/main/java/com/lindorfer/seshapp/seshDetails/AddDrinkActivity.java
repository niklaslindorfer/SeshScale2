package com.lindorfer.seshapp.seshDetails;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;

import com.google.gson.Gson;
import com.lindorfer.seshapp.R;
import com.lindorfer.seshapp.classes.Drink;
import com.lindorfer.seshapp.classes.SeshUser;
import com.lindorfer.seshapp.classes.UserProfile;
import com.lindorfer.seshapp.exceptions.QueryException;
import com.lindorfer.seshapp.queries.QueryCaller;
import com.lindorfer.seshapp.queries.QueryManager;
import com.lindorfer.seshapp.queries.RequestType;
import com.lindorfer.seshapp.queries.RequestURL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

public class AddDrinkActivity extends AppCompatActivity implements QueryCaller {

    private SeshUser seshUser;
    private SharedPreferences sharedPref;
    private UserProfile user;
    private Drink selected = null;
    private ListView lvDrinks;
    private ArrayAdapter<Drink> itemsAdapter;
    private List<Drink> drinks;
    private FloatingActionButton btSubmitDrinkLog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_drink);

        // Get sesh from Intent call
        Intent intent = getIntent();
        String argument = intent.getStringExtra("sesh_user");

        // Parse to object
        Gson gson = new Gson();
        seshUser = gson.fromJson(argument, SeshUser.class);
        System.out.println("SESH USER RETRIEVED: " + seshUser);

        // Toolbar
        Toolbar tbNewDrinkLog = findViewById(R.id.tbNewDrinkLog);
        tbNewDrinkLog.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        tbNewDrinkLog.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tbNewDrinkLog.setTitleTextColor(Color.WHITE);

        // Get current user TODO: maybe refactor
        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        this.user = gson.fromJson(sharedPref.getString("user_profile", null), UserProfile.class);

        // Set list view
        lvDrinks = findViewById(R.id.lvDrinks);
        drinks = new ArrayList<>();

        // Set list view adapter
        itemsAdapter = new DrinkListAdapter(this, drinks);
        lvDrinks.setAdapter(itemsAdapter);

        // Set search bar
        EditText sDrinkSearch = findViewById(R.id.sDrinkSearch);
        sDrinkSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                itemsAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        // Show keyboard
        sDrinkSearch.requestFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        // Floating action button to create new drink log
        btSubmitDrinkLog = this.findViewById(R.id.btSubmitDrinkLog);
        btSubmitDrinkLog.setVisibility(View.INVISIBLE);
        btSubmitDrinkLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitDrinkLog(v);
            }
        });
        getDrinks();
    }

    private void getDrinks(){
        // Online server request
        try{
            // Send request
            QueryManager q = new QueryManager(RequestType.POST, user, RequestURL.GET_DRINK_LIST, "", this);
            q.execute();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (QueryException e) {
            e.printStackTrace();
        }
    }

    public void submitDrinkLog(View view){
        // Online server request
        try{
            JSONObject request = new JSONObject();
            request.put("sesh", seshUser.getSesh().getId());
            request.put("user", seshUser.getUser().getUsername());
            request.put("drink", selected.getId());

            QueryManager q = new QueryManager(RequestType.POST, user, RequestURL.ADD_DRINK_LOG, request.toString(), this);
            q.execute();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (QueryException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getServerResponse(String response) {
        if (response.equals("SUCCESS")){
            finish();
        } else {
            // Get drinks list
            try{
                JSONArray responseArray = new JSONArray(response);
                for (int i = 0; i < responseArray.length(); i++) {
                    JSONObject element = responseArray.getJSONObject(i);

                    System.out.println(element.toString());
                    int id = Integer.parseInt(element.getString("id"));
                    double units = Double.parseDouble(element.getString("units"));
                    String name = element.getString("name");
                    String type = element.getString("drink_type");

                    Drink drink = new Drink(id, name, units, type);
                    drinks.add(drink);
                }

                lvDrinks.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        // Get selected item
                        selected = itemsAdapter.getItem(position);
                        btSubmitDrinkLog.setVisibility(View.VISIBLE);
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    @Override
    public void showProgress(final Boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);


            final ProgressBar mProgressView = findViewById(R.id.login_progress);
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });

            final ListView lvDrinks = findViewById(R.id.lvDrinks);
            lvDrinks.setVisibility(show ? View.GONE : View.VISIBLE);
            lvDrinks.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    lvDrinks.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });
        }
    }
}
