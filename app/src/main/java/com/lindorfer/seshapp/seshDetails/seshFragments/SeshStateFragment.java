package com.lindorfer.seshapp.seshDetails.seshFragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.lindorfer.seshapp.queries.RequestURL;
import com.lindorfer.seshapp.seshDetails.AddStateActivity;
import com.lindorfer.seshapp.classes.SeshUser;
import com.lindorfer.seshapp.classes.StateLog;
import com.lindorfer.seshapp.exceptions.QueryException;
import com.lindorfer.seshapp.main.mainFragments.MainFragment;
import com.lindorfer.seshapp.queries.QueryCaller;
import com.lindorfer.seshapp.queries.QueryManager;
import com.lindorfer.seshapp.queries.RequestType;
import com.lindorfer.seshapp.R;
import com.lindorfer.seshapp.seshDetails.StateLogListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SeshStateFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SeshStateFragment extends MainFragment implements QueryCaller{

    private static final String ARG_SESH = "sesh_arg";
    private SeshUser seshUser;

    private ListView lvStateLogs;
    private GraphView gvStateLogs;
    private List<StateLog> stateLogs;
    private ArrayAdapter<StateLog> itemsAdapter;

    public SeshStateFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param sesh SeshUser
     * @return A new instance of fragment SeshStateFragment.
     */
    public static SeshStateFragment newInstance(String sesh) {
        SeshStateFragment fragment = new SeshStateFragment();
        Bundle args = new Bundle();
        args.putString(ARG_SESH, sesh);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Gson gson = new Gson();
            seshUser = gson.fromJson(getArguments().getString(ARG_SESH), SeshUser.class);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sesh_state, container, false);
    }

    @Override
    public void onResume(){
        super.onResume();

        Gson gson = new Gson();
        seshUser = gson.fromJson(getArguments().getString(ARG_SESH), SeshUser.class);

        lvStateLogs = getActivity().findViewById(R.id.lvStateLogs);

        stateLogs = new ArrayList<>();
        getStateLogs();

    }

    private void getStateLogs(){
        // Online server request
        try{
            // Prepare request body
            JSONObject request = new JSONObject();
            request.put("username", user.getUsername());
            request.put("sesh", seshUser.getSesh().getId());

            // Send request
            QueryManager q = new QueryManager(RequestType.POST, user, RequestURL.GET_STATE_LOGS, request.toString(), this);
            q.execute();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (QueryException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getServerResponse(String response) {
        try {
            JSONArray responseArray = new JSONArray(response);
            for (int i = 0; i < responseArray.length(); i++) {
                JSONObject element = responseArray.getJSONObject(i).getJSONObject("fields");

                System.out.println(element.toString());
                int physical = Integer.parseInt(element.getString("physical"));
                int mental = Integer.parseInt(element.getString("mental"));
                String dateTime = element.getString("date_time");

                stateLogs.add(new StateLog(dateTime, physical, mental));
            }

            // Set adapter
            itemsAdapter = new StateLogListAdapter(getActivity(), stateLogs);
            lvStateLogs.setAdapter(itemsAdapter);

            // Set graph
            gvStateLogs = getActivity().findViewById(R.id.gvStateLogs);
            LineGraphSeries<DataPoint> physicalSeries = new LineGraphSeries<>();
            LineGraphSeries<DataPoint> mentalSeries = new LineGraphSeries<>();
            for (StateLog log : stateLogs) {
                physicalSeries.appendData(new DataPoint(log.getDate().getMillis(), log.getPhysical()), true, stateLogs.size());
                mentalSeries.appendData(new DataPoint(log.getDate().getMillis(), log.getMental()), true, stateLogs.size());
            }

            gvStateLogs.addSeries(physicalSeries);
            gvStateLogs.addSeries(mentalSeries);

            // Graph formatting
            physicalSeries.setThickness(12);
            physicalSeries.setColor(Color.BLACK);
            physicalSeries.setAnimated(true);

            mentalSeries.setThickness(12);
            mentalSeries.setColor(Color.CYAN);
            mentalSeries.setAnimated(true);

            gvStateLogs.getLegendRenderer().setVisible(false);
            gvStateLogs.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
                @Override
                public String formatLabel(double value, boolean isValueX) {
                    if (isValueX)
                        return "";
                    else
                        return super.formatLabel(value, isValueX);
                }
            });
            gvStateLogs.getGridLabelRenderer().setGridStyle(GridLabelRenderer.GridStyle.HORIZONTAL);
            gvStateLogs.getGridLabelRenderer().setGridColor(Color.BLACK);

            // Floating action button to create new state log
            FloatingActionButton btNewState = getActivity().findViewById(R.id.btNewState);
            btNewState.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent newStateIntent = new Intent(getActivity(), AddStateActivity.class);
                    Gson gson = new Gson();
                    newStateIntent.putExtra("sesh_user", gson.toJson(seshUser, SeshUser.class));
                    startActivity(newStateIntent);
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    @Override
    public void showProgress(final Boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);


            final ProgressBar mProgressView = getActivity().findViewById(R.id.login_progress);
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });

            final ViewPager vpSeshTabs = getActivity().findViewById(R.id.vpSeshTabs);
            vpSeshTabs.setVisibility(show ? View.GONE : View.VISIBLE);
            vpSeshTabs.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    vpSeshTabs.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });
        }

    }
}
