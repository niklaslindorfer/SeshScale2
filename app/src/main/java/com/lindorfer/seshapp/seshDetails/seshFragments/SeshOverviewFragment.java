package com.lindorfer.seshapp.seshDetails.seshFragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import com.google.gson.Gson;
import com.lindorfer.seshapp.classes.SeshUser;
import com.lindorfer.seshapp.exceptions.QueryException;
import com.lindorfer.seshapp.main.mainFragments.MainFragment;
import com.lindorfer.seshapp.R;
import com.lindorfer.seshapp.queries.QueryCaller;
import com.lindorfer.seshapp.queries.QueryManager;
import com.lindorfer.seshapp.queries.RequestType;
import com.lindorfer.seshapp.queries.RequestURL;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SeshOverviewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SeshOverviewFragment extends MainFragment implements QueryCaller{

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_SESH = "sesh_arg";
    private SeshUser seshUser;

    private TextView lbHost;
    private Switch swChunder;
    private Switch swBlackout;

    public SeshOverviewFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param sesh SeshUser
     * @return A new instance of fragment SeshOverviewFragment.
     */
    public static SeshOverviewFragment newInstance(String sesh) {
        SeshOverviewFragment fragment = new SeshOverviewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_SESH, sesh);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Gson gson = new Gson();
            seshUser = gson.fromJson(getArguments().getString(ARG_SESH), SeshUser.class);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sesh_overview, container, false);
    }

    @Override
    public void onResume(){
        super.onResume();

        System.out.println(">>> SESH OVERVIEW FRAGMENT");

        Gson gson = new Gson();
        seshUser = gson.fromJson(getArguments().getString(ARG_SESH), SeshUser.class);


        lbHost = getActivity().findViewById(R.id.lbHost);
        swChunder = getActivity().findViewById(R.id.swChunder);
        swBlackout = getActivity().findViewById(R.id.swBlackout);

        lbHost.setText(seshUser.isHost() ? "You are a host." : "You are not a host.");
        swChunder.setChecked(seshUser.isChunder());
        swBlackout.setChecked(seshUser.isBlackout());

        // Set listeners for switches
        swChunder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stateChanged(v);
            }
        });
        swBlackout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stateChanged(v);
            }
        });

    }

    private void stateChanged(View view){
        System.out.println("STATE CHANGED");

        // Online server request
        try{
            // Prepare request body
            JSONObject request = new JSONObject();
            request.put("username", user.getUsername());
            request.put("sesh", seshUser.getSesh().getId());
            request.put("chunder", swChunder.isChecked());
            request.put("blackout", swBlackout.isChecked());

            System.out.println("REQUEST: " + request.toString());

            // Send request
            QueryManager q = new QueryManager(RequestType.POST, user, RequestURL.UPDATE_SESH, request.toString(), this);
            q.execute();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (QueryException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getServerResponse(String response) {
        if (!response.equals("SUCCESS")){
            // Show toast
            Snackbar.make(getView(), "Whoops, something went wrong.", Snackbar.LENGTH_SHORT)
                    .setAction("Action", null).show();
        }

    }

    @Override
    public void showProgress(final Boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);


            final ProgressBar mProgressView = getActivity().findViewById(R.id.login_progress);
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });

            final ViewPager vpSeshTabs = getActivity().findViewById(R.id.vpSeshTabs);
            vpSeshTabs.setVisibility(show ? View.GONE : View.VISIBLE);
            vpSeshTabs.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    vpSeshTabs.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });
        }
    }
}
