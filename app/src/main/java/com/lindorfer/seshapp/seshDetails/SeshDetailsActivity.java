package com.lindorfer.seshapp.seshDetails;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.google.gson.Gson;
import com.lindorfer.seshapp.classes.SeshUser;
import com.lindorfer.seshapp.R;
import com.lindorfer.seshapp.seshDetails.seshFragments.SeshDrinkFragment;
import com.lindorfer.seshapp.seshDetails.seshFragments.SeshOverviewFragment;
import com.lindorfer.seshapp.seshDetails.seshFragments.SeshStateFragment;

public class SeshDetailsActivity extends AppCompatActivity {

    private SeshUser seshUser;
    Toolbar tbSeshDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sesh_details);

        // Get sesh from Intent call
        Intent intent = getIntent();
        String argument = intent.getStringExtra("sesh_user");

        // Parse to object
        Gson gson = new Gson();
        seshUser = gson.fromJson(argument, SeshUser.class);
        System.out.println("SESH USER RETRIEVED: " + seshUser);

        // Setup toolbar
        tbSeshDetails = findViewById(R.id.tbSeshDetails);
        tbSeshDetails.setTitle(seshUser.getSesh().getDescription());
        tbSeshDetails.setSubtitle(seshUser.getSesh().getId() + " - " + seshUser.getSesh().getLocation());
        tbSeshDetails.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        tbSeshDetails.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tbSeshDetails.setTitleTextColor(Color.WHITE);
        tbSeshDetails.setSubtitleTextColor(Color.WHITE);

        // Get the ViewPager and set it's PagerAdapter so that it can display items
        ViewPager viewPager = findViewById(R.id.vpSeshTabs);
        viewPager.setAdapter(new SeshFragmentPagerAdapter(getSupportFragmentManager(), seshUser, this));

        // Give the TabLayout the ViewPager
        TabLayout tabLayout = findViewById(R.id.tlSeshTabs);
        tabLayout.setupWithViewPager(viewPager);

    }

    class SeshFragmentPagerAdapter extends FragmentPagerAdapter {
        final int PAGE_COUNT = 3;
        private String tabTitles[] = new String[] { "overview", "drinks", "states" };
        private Context context;
        private SeshUser seshUser;

        public SeshFragmentPagerAdapter(FragmentManager fm, SeshUser seshUser, Context context) {
            super(fm);
            this.context = context;
            this.seshUser = seshUser;
        }

        public SeshFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
            //this.context = context;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public Fragment getItem(int position) {
            Gson gson = new Gson();
            String seshUserString = gson.toJson(seshUser);
            System.out.println("POSITION: " + position + ", " + getPageTitle(position));
            switch(position){
                case 2:
                    return SeshStateFragment.newInstance(seshUserString);
                case 1:
                    return SeshDrinkFragment.newInstance(seshUserString);
                case 0:
                    return SeshOverviewFragment.newInstance(seshUserString);
                default:
                    return SeshDrinkFragment.newInstance("");

            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            return tabTitles[position];
        }
    }

}
