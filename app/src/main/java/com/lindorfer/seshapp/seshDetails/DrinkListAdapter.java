package com.lindorfer.seshapp.seshDetails;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.lindorfer.seshapp.R;
import com.lindorfer.seshapp.classes.Drink;
import com.lindorfer.seshapp.classes.SeshUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nikla on 31/03/2018.
 */

public class DrinkListAdapter extends ArrayAdapter<Drink> implements Filterable {
    private final Context context;
    private List<Drink> drinkList;
    private List<Drink> deepDrinkList;
    private ItemFilter mFilter = new ItemFilter();
    private View rowView;

    public DrinkListAdapter(Context context, List<Drink> drinkList) {
        super(context, -1, drinkList);
        this.context = context;
        this.drinkList = drinkList;
        this.deepDrinkList = drinkList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        rowView = inflater.inflate(R.layout.drink_list_row_layout, parent, false);


        TextView lbDrinkName = rowView.findViewById(R.id.lbDrinkName);
        TextView lbUnits = rowView.findViewById(R.id.lbUnits);
        TextView lbType = rowView.findViewById(R.id.lbType);
        ImageView ivDrinkIcon = rowView.findViewById(R.id.ivDrinkIcon);

        Drink item = drinkList.get(position);

        lbDrinkName.setText(item.getName());
        lbUnits.setText(item.getUnits() + " UK units");
        lbType.setText("Type: " + item.getType());

        // Icon
        if (item.getType().equals("pint"))
            ivDrinkIcon.setImageResource(R.drawable.ic_beer);
        else if (item.getType().equals("mix"))
            ivDrinkIcon.setImageResource(R.drawable.ic_cocktail);
        else if (item.getType().equals("can"))
            ivDrinkIcon.setImageResource(R.drawable.ic_can);
        else if (item.getType().equals("bottle"))
            ivDrinkIcon.setImageResource(R.drawable.ic_bottle);
        else if (item.getType().equals("shot"))
            ivDrinkIcon.setImageResource(R.drawable.ic_shot);

        return rowView;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }
    @Override
    public int getCount() {
        return drinkList.size();
    }
    @Override
    public Drink getItem(int position) {
        return drinkList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ItemFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();
            FilterResults results = new FilterResults();

            final List<Drink> list = deepDrinkList;
            final ArrayList<Drink> nlist = new ArrayList<>();

            for (Drink filtDrink : list) {
                if (filtDrink.getName().toLowerCase().contains(filterString))
                    nlist.add(filtDrink);
            }

            results.values = nlist;
            results.count = nlist.size();
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            drinkList = (ArrayList<Drink>) results.values;
            notifyDataSetChanged();
        }

    }


}
