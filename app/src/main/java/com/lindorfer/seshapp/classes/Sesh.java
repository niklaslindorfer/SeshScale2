package com.lindorfer.seshapp.classes;

/**
 * Created by nikla on 31/03/2018.
 */

public class Sesh {

    private int id;
    private String description;
    private String location;
    private String date;

    public Sesh(int id, String description, String location, String date) {
        this.id = id;
        this.description = description;
        this.location = location;
        this.date = date;
    }

    public Sesh(int id, String description, String location) {
        this.id = id;
        this.description = description;
        this.location = location;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString(){
        return "[" + id + "] " + description + " @ " + location;
    }
}
