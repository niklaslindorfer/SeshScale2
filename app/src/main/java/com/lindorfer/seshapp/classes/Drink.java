package com.lindorfer.seshapp.classes;

/**
 * Created by nikla on 31/03/2018.
 */

public class Drink {

    private int id;
    private String name;
    private double units;
    private String type;

    public Drink(int id, String name, double units) {
        this.id = id;
        this.name = name;
        this.units = units;
    }

    public Drink(int id, String name, double units, String type) {
        this.id = id;
        this.name = name;
        this.units = units;
        this.type = type;
    }

    public Drink(String name, double units) {
        this.name = name;
        this.units = units;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getUnits() {
        return units;
    }

    public void setUnits(double units) {
        this.units = units;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString(){
        return name + " (" + units + ")";
    }
}
