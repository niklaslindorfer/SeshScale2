package com.lindorfer.seshapp.classes;

import java.util.List;

/**
 * Created by nikla on 31/03/2018.
 */

public class SeshUser {

    private Sesh sesh;
    private UserProfile user;
//    private List<DrinkLog> drinkLogs;
//    private List<StateLog> stateLogs;
    private boolean host;
    private boolean chunder;
    private boolean blackout;
    private boolean active;

//    public SeshUser(Sesh sesh, UserProfile user, List<DrinkLog> drinkLogs, List<StateLog> stateLogs, boolean host, boolean chunder, boolean blackout, boolean active) {
//        this.sesh = sesh;
//        this.user = user;
//        this.drinkLogs = drinkLogs;
//        this.stateLogs = stateLogs;
//        this.host = host;
//        this.chunder = chunder;
//        this.blackout = blackout;
//        this.active = active;
//    }

    public SeshUser(Sesh sesh, UserProfile user, boolean host, boolean chunder, boolean blackout, boolean active) {
        this.sesh = sesh;
        this.user = user;
        this.host = host;
        this.chunder = chunder;
        this.blackout = blackout;
        this.active = active;
    }

    public SeshUser(Sesh sesh, UserProfile user) {
        this.sesh = sesh;
        this.user = user;
    }

    public Sesh getSesh() {
        return sesh;
    }

    public void setSesh(Sesh sesh) {
        this.sesh = sesh;
    }

    public UserProfile getUser() {
        return user;
    }

    public void setUser(UserProfile user) {
        this.user = user;
    }

//    public List<DrinkLog> getDrinkLogs() {
//        return drinkLogs;
//    }
//
//    public void setDrinkLogs(List<DrinkLog> drinkLogs) {
//        this.drinkLogs = drinkLogs;
//    }
//
//    public List<StateLog> getStateLogs() {
//        return stateLogs;
//    }
//
//    public void setStateLogs(List<StateLog> stateLogs) {
//        this.stateLogs = stateLogs;
//    }

    public boolean isHost() {
        return host;
    }

    public void setHost(boolean host) {
        this.host = host;
    }

    public boolean isChunder() {
        return chunder;
    }

    public void setChunder(boolean chunder) {
        this.chunder = chunder;
    }

    public boolean isBlackout() {
        return blackout;
    }

    public void setBlackout(boolean blackout) {
        this.blackout = blackout;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString(){
        return user.getUsername() + ": "+ sesh;
    }
}
