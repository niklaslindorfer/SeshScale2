package com.lindorfer.seshapp.classes;

/**
 * Created by nikla on 31/03/2018.
 */

public class StateLog extends Log{

    private int physical;
    private int mental;
    private UserProfile ratedBy;

    public StateLog(String dateTime, int physical, int mental, UserProfile ratedBy) {
        super(dateTime);
        this.physical = physical;
        this.mental = mental;
        this.ratedBy = ratedBy;
    }

    public StateLog(String dateTime, int physical, int mental) {
        super(dateTime);
        this.physical = physical;
        this.mental = mental;
    }

    public StateLog(int physical, int mental) {
        super();
        this.physical = physical;
        this.mental = mental;
    }

    public int getPhysical() {
        return physical;
    }

    public void setPhysical(int physical) {
        this.physical = physical;
    }

    public int getMental() {
        return mental;
    }

    public void setMental(int mental) {
        this.mental = mental;
    }

    public UserProfile getRatedBy() {
        return ratedBy;
    }

    public void setRatedBy(UserProfile ratedBy) {
        this.ratedBy = ratedBy;
    }

    @Override
    public String toString(){
        return "Physical: " + physical + ", Mental: " + mental;
    }
}
