package com.lindorfer.seshapp.classes;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created by nikla on 02/04/2018.
 */

public class Log {
    protected int id;
    protected String dateTime;
    protected DateTime date;

    public Log(int id, String dateTime) {
        this.id = id;
        this.dateTime = dateTime;
        date = parseDateTime(dateTime);
    }

    public Log(String dateTime) {
        this.dateTime = dateTime;
        date = parseDateTime(dateTime);
    }

    public Log(){}

    private DateTime parseDateTime(String dT){
        // TODO: This is an utter mess.
        String wtfIsThis = dT.replace("T", " ").substring(0,19);
        DateTimeFormatter f = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        return f.parseDateTime(wtfIsThis).plusMinutes(59);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }
}
