package com.lindorfer.seshapp.classes;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * Created by nikla on 31/03/2018.
 */

public class DrinkLog extends Log{

    private Drink drink;
    private Date date;

    public DrinkLog(Drink drink, String dateTime, int id) {
        super(id, dateTime);
        this.drink = drink;
    }

    public DrinkLog(Drink drink, String dateTime) {
        super(dateTime);
        this.drink = drink;
    }

    public Drink getDrink() {
        return drink;
    }

    public void setDrink(Drink drink) {
        this.drink = drink;
    }

    @Override
    public String toString(){
        return drink + " - " + dateTime;
    }
}
